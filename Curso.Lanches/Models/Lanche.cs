﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Curso.Lanches.Models
{
    public class Lanche
    {
        [Key]
        public int lancheID { get; set; }
        [StringLength(100)]
        public string nome { get; set; }
        [StringLength(100)]
        public string descricaoCurta { get; set; }
        [StringLength(255)]
        public string descricaoDetalhada { get; set; }
        public float preco { get; set; }
        [StringLength(200)]
        public string imagemUrl { get; set; }
        [StringLength(200)]
        public string imagemThumbnailUrl { get; set; }
        public bool lanchePreferido { get; set; }
        public bool emEstoque { get; set; }
        public int categoriaID { get; set; }
        public virtual Categoria categoria { get; set; }
    }
}
