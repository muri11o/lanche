﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Curso.Lanches.Models
{
    public class Categoria
    {
        [Key]
        public int categoriaID { get; set; }
        [StringLength(100)]
        public string categoriaNome { get; set; }
        public string descricao { get; set; }
        public List <Lanche> listaLanches { get; set; }
    }
}
