﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Curso.Lanches.Context;
using Curso.Lanches.Models;

namespace Curso.Lanches.Repositories
{
    public class CategoriaRepository : ICategoriaRepository
    {
        private readonly LancheContext _context;
        public CategoriaRepository(LancheContext contexto)
        {
            _context = contexto;
        }
        public IEnumerable<Categoria> Categorias => _context.Categorias;
    }
}
