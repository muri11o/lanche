﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Curso.Lanches.Migrations
{
    public partial class CarrinhoCmpraIte : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CarrinhoCompraItens",
                columns: table => new
                {
                    carrinhoCompraItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    lancheID = table.Column<int>(nullable: true),
                    quantidade = table.Column<int>(nullable: false),
                    carrinhoCompraID = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarrinhoCompraItens", x => x.carrinhoCompraItemID);
                    table.ForeignKey(
                        name: "FK_CarrinhoCompraItens_lanches_lancheID",
                        column: x => x.lancheID,
                        principalTable: "lanches",
                        principalColumn: "lancheID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarrinhoCompraItens_lancheID",
                table: "CarrinhoCompraItens",
                column: "lancheID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarrinhoCompraItens");
        }
    }
}
