﻿using Curso.Lanches.Models;
using Microsoft.EntityFrameworkCore;

namespace Curso.Lanches.Context
{
    public class LancheContext : DbContext
    {
        public LancheContext(DbContextOptions<LancheContext> options) : base(options)
        {

        }

        public DbSet<Lanche> lanches { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<CarrinhoCompraItem> CarrinhoCompraItens { get; set;}
    }
}
