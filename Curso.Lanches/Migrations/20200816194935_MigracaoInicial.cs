﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Curso.Lanches.Migrations
{
    public partial class MigracaoInicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    categoriaID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    categoriaNome = table.Column<string>(maxLength: 100, nullable: true),
                    descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.categoriaID);
                });

            migrationBuilder.CreateTable(
                name: "lanches",
                columns: table => new
                {
                    lancheID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    nome = table.Column<string>(maxLength: 100, nullable: true),
                    descricaoCurta = table.Column<string>(maxLength: 100, nullable: true),
                    descricaoDetalhada = table.Column<string>(maxLength: 255, nullable: true),
                    preco = table.Column<float>(nullable: false),
                    imagemUrl = table.Column<string>(maxLength: 200, nullable: true),
                    imagemThumbnailUrl = table.Column<string>(maxLength: 200, nullable: true),
                    lanchePreferido = table.Column<bool>(nullable: false),
                    emEstoque = table.Column<bool>(nullable: false),
                    categoriaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lanches", x => x.lancheID);
                    table.ForeignKey(
                        name: "FK_lanches_Categorias_categoriaID",
                        column: x => x.categoriaID,
                        principalTable: "Categorias",
                        principalColumn: "categoriaID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_lanches_categoriaID",
                table: "lanches",
                column: "categoriaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "lanches");

            migrationBuilder.DropTable(
                name: "Categorias");
        }
    }
}
