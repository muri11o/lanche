﻿using Curso.Lanches.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Curso.Lanches.Models
{
    public class CarrinhoCompra
    {
        private readonly LancheContext _context;

        public CarrinhoCompra(LancheContext contexto)
        {
            _context = contexto;
        }

        public string _carrinhoCompraID { get; set; }
        public List<CarrinhoCompraItem> carrinhoCompraItens { get; set; }

        public static CarrinhoCompra GetCarrinho(System.IServiceProvider services)
        {
            //define uma sessão acessando o contexto atual (tem que registrar em IServicesCollection
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session; //Operador '?' signigica: se houver retorna, senão retorna nulo

            //obtem um serviço do tipo do nosso contexto
            var context = services.GetService<LancheContext>();

            // obtem ou gera o ID do carrinho
            string carrinhoID = session.GetString("carrinhoID") ?? Guid.NewGuid().ToString(); // Operador '??' significa: se tiver, retorna ele senão cria um novo

            // atribui o ID do carrinho na sessão
            session.SetString("carrinhoID", carrinhoID);

            //retorna o carrinho com o contexto atual e o ID atribuido ou obtido
            return new CarrinhoCompra(context)
            {
                _carrinhoCompraID = carrinhoID
            };   
        }

        public void AdicionarAoCarrinho(Lanche _lanche, int _quantidade)
        {
            var carrinhoCompraItem = _context.CarrinhoCompraItens.SingleOrDefault(s => s.lanche.lancheID == _lanche.lancheID && s.carrinhoCompraID == _carrinhoCompraID);

            //verificar se o carrinho existe e senão existir criar um
            if (carrinhoCompraItem == null)
            {
                carrinhoCompraItem = new CarrinhoCompraItem
                {
                    carrinhoCompraID = _carrinhoCompraID,
                    lanche = _lanche,
                    quantidade = 1
                    
                };
                _context.CarrinhoCompraItens.Add(carrinhoCompraItem);
            }
            else // se existir o carrinho com o item entãi incrementa a quantidade
            {
                carrinhoCompraItem.quantidade++;
            }
            _context.SaveChanges();
        }

        public int RemoverDoCarrinho(Lanche _lanche)
        {
            var carrinhoCompraItem = _context.CarrinhoCompraItens.SingleOrDefault(s => s.lanche.lancheID == _lanche.lancheID && s.carrinhoCompraID == _carrinhoCompraID);

            var quantidadeLocal = 0;

            if (carrinhoCompraItem != null)
            {
                if (carrinhoCompraItem.quantidade > 1)
                {
                    carrinhoCompraItem.quantidade--;
                    quantidadeLocal = carrinhoCompraItem.quantidade;
                }
                else
                {
                    _context.CarrinhoCompraItens.Remove(carrinhoCompraItem);
                }
            }

            _context.SaveChanges();
            return quantidadeLocal;
        }

        public List<CarrinhoCompraItem> GetCarrinhoCompraItens()
        {
            return carrinhoCompraItens ?? (carrinhoCompraItens =_context.CarrinhoCompraItens.Where(c => c.carrinhoCompraID == _carrinhoCompraID).Include(s => s.lanche).ToList());
        }

        public void LimparCarrinho()
        {
            var carrinhoItens = _context.CarrinhoCompraItens.Where(carrinho => carrinho.carrinhoCompraID == _carrinhoCompraID);

            _context.CarrinhoCompraItens.RemoveRange(carrinhoItens);

            _context.SaveChanges();
        }   

        public float GetCarrinhoCompraTotal()
        {
            var total = _context.CarrinhoCompraItens.Where(c => c.carrinhoCompraID == _carrinhoCompraID).Select(c => c.lanche.preco * c.quantidade).Sum();
            return total;
        }
    }
}
