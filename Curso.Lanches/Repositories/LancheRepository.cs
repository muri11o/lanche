﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Curso.Lanches.Context;
using Curso.Lanches.Models;
using Microsoft.EntityFrameworkCore;

namespace Curso.Lanches.Repositories
{
    public class LancheRepository : ILancheRepository
    {

        private readonly LancheContext _context;

        public LancheRepository(LancheContext contexto)
        {
            _context = contexto;
        }

        public IEnumerable<Lanche> Lanches => _context.lanches.Include(c => c.categoria);

        public IEnumerable<Lanche> LanchesPreferidos => _context.lanches.Where(p => p.lanchePreferido).Include(c => c.categoria);

        public Lanche getLanchePeloID(int lancheID)
        {
            return _context.lanches.FirstOrDefault(l => l.lancheID == lancheID);
        }
    }
}
