﻿using System.ComponentModel.DataAnnotations;

namespace Curso.Lanches.Models
{
    public class CarrinhoCompraItem
    {
        [Key]
        public int carrinhoCompraItemID { get; set; }
        public Lanche lanche { get; set; }
        public int quantidade { get; set; }
        [StringLength(200)]
        public string carrinhoCompraID { get; set; }
    }
}
